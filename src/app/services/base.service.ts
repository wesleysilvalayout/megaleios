import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class BaseService {
  url:string = "http://api.template.megaleios.com/api/v1/profile/"
  constructor(private http: HttpClient) { }

  setDado(tabela: any, dados: any) {
    return this.http.post(`${this.url}/${tabela}/`, dados)
  }
  getDados(tabela: any) {
    return this.http.get(`${this.url}/${tabela}/`)
  }
}
