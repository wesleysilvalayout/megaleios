import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BaseService } from 'src/app/services/base.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  formConfig: FormGroup;
  constructor(
    private fb: FormBuilder,
    private base: BaseService,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit() {
    this.formConfig = this.fb.group({
      login: ['', [Validators.minLength(6), Validators.maxLength(16)]],
      password: ['', [Validators.minLength(6), Validators.maxLength(16)]],
      fullName: ['', [Validators.minLength(6), Validators.maxLength(100)]],
      email: ['', [Validators.email]],
      cpf:[''],
      phone: ['']
    })

  }
  submmit() {
    console.log(this.formConfig)
    this.base.setDado('Register', this.formConfig.value).subscribe((res: any) => {
      console.log(res)
      localStorage.setItem('access_token', res.data.access_token)
      localStorage.setItem('refresh_token', res.data.refresh_token)

      this.router.navigate(['/list']);

    }, erro => {
      //console.log(erro)
    })
  }
  voltar(){
    this.location.back()
  }

}
